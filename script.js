/**
 * RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW
 * TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
 * LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT:
 * https://disqus.com/admin/universalcode/#configuration-variables
 */

var disqus_config = function () {
  this.page.url = "https://amigogilvan.netlify.com/"; // Replace PAGE_URL with your page's canonical URL variable
  this.page.identifier = 1; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};

(function () {
  // DON'T EDIT BELOW THIS LINE
  var d = document,
    s = d.createElement("script");

  s.src = "https://amigogilvan.disqus.com/embed.js";
  s.setAttribute("data-timestamp", +new Date());

  (d.head || d.body).appendChild(s);
})();

(function () {
  const spanElement = window.document.getElementById("radioStatus");

  axios
    .head("https://s02.maxcast.com.br:8188/live")
    .then(function (response) {
      spanElement.innerText = "Online";
      spanElement.className = "text-success";
    })
    .catch(function (err) {
      spanElement.innerText = "Offline";
      spanElement.className = "text-danger";
    });
})();
